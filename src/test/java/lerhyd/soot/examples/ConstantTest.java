package lerhyd.soot.examples;

import lerhyd.Solution;
import org.junit.Test;
import soot.*;
import soot.jimple.Constant;

import java.util.Map;

public class ConstantTest extends SceneTransformer {
	
	private ConstantAnalysis analysis;

	
	@Override
	protected void internalTransform(String arg0, @SuppressWarnings("rawtypes") Map arg1) {
		analysis = new ConstantAnalysis();
		analysis.doAnalysis();
		Solution<Unit,Map<Local,Constant>> solution = analysis.getMeetSolution();
		System.out.println("*********************************************");
		for (SootMethod sootMethod : analysis.getMethods()) {
			System.out.println(sootMethod);
			for (Unit unit : sootMethod.getActiveBody().getUnits()) {
				System.out.println("*********************************************");
				System.out.println(unit);
				System.out.println("IN:  " + formatConstants(solution.getValueBefore(unit)));
				System.out.println("OUT: " + formatConstants(solution.getValueAfter(unit)));
			}
			System.out.println("*********************************************");
		}		
	}
	
	public static String formatConstants(Map<Local, Constant> value) {
		if (value == null) {
			return "";
		}
		StringBuffer sb = new StringBuffer();
		for (Map.Entry<Local,Constant> entry : value.entrySet()) {
			Local local = entry.getKey();
			Constant constant = entry.getValue();
			if (constant != null) {
				sb.append("(").append(local).append("=").append(constant).append(") ");
			}
		}
		return sb.toString();
	}

	public static void main(String args[]) {
		String classPath = System.getProperty("java.class.path");
		String mainClass = null;

		try {
			int i=0;
			while(true){
				if (args[i].equals("-cp")) {
					classPath = args[i+1];
					i += 2;
				} else {
					mainClass = args[i];
					i++;
					break;
				}
			}
			if (i != args.length || mainClass == null)
				throw new Exception();
		} catch (Exception e) {
			System.err.println("Usage: java CopyConstantTest [-cp CLASSPATH] MAIN_CLASS");
			System.exit(1);
		}
		
		String[] sootArgs = {
				"-cp", classPath, "-pp", 
				"-w", "-app", 
				"-keep-line-number",
				"-keep-bytecode-offset",
				"-p", "jb", "use-original-names",
				"-p", "cg", "implicit-entry:false",
				"-p", "cg.spark", "enabled",
				"-p", "cg.spark", "simulate-natives",
				"-p", "cg", "safe-forname",
				"-p", "cg", "safe-newinstance",
				"-main-class", mainClass,
				"-f", "none", mainClass 
		};
		ConstantTest ct = new ConstantTest();
		PackManager.v().getPack("wjtp").add(new Transform("wjtp.ccp", ct));
		soot.Main.main(sootArgs);
	}

	@Test
	public void testCopyConstantAnalysis() {
		ConstantTest.main(new String[]{"lerhyd.tests.ConstantCase"});
	}
	
}
