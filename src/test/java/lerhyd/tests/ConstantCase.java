package lerhyd.tests;

public class ConstantCase {
	public static void main(String... args) {
		Science science = new Science(8);
		int x = science.thirdM(66,100);
		int y = science.firstM();
		int z = science.secondM();
		System.out.println(x+y+z);
	}
	
	private static class Science {
		int value;
		
		Science(int value) {
			this.value = value;
		}
		
		public int firstM() {
			return value*value*value;
		}
		
		public int secondM() {
			return this.value;
		}
		
		public int thirdM(int firstOp, int seconOp) {
			int x = firstOp;
			int y = seconOp;
			int z;
			if (firstOp < 5) {
				z = x;
			} else {
				z = y;
			}
			return z;
		}
	}
}
