package lerhyd;

import lerhyd.util.AnalysisUtil;

import java.util.*;

public abstract class OldAnalysis<Method,Node,ValueType> extends Analysis<Method,Node,ValueType> {

	private AnalysisUtil analysisUtil;

	public OldAnalysis() {
		super(false);
		analysisStack = new Stack<>();
		analysisUtil = new AnalysisUtil();
	}
	
	protected Stack<Context<Method,Node,ValueType>> analysisStack;

	@Override
	public void doAnalysis() {
		for (Method entryPoint : representation().getEntryPoints()) {
			Context<Method,Node,ValueType> context = new Context<>(entryPoint, representation().getCtrlGraph(entryPoint), false);
			ValueType boundaryInformation = edgeValue(entryPoint);
			initContext(context, boundaryInformation);
		}

		while (!analysisStack.isEmpty()) {
			Context<Method, Node,ValueType> context = analysisStack.peek();
			if (!context.getAnalyseList().isEmpty()) {
				Node nodeUnit = context.getAnalyseList().pollFirst();

				if (nodeUnit != null) {
					List<Node> predecessors = context.getCtrlGraph().getPredsOf(nodeUnit);
					if (predecessors.size() != 0) {
						Iterator<Node> predecessorIterator = predecessors.iterator();
						ValueType inValue = context.getValueAfter(predecessorIterator.next());
						while (predecessorIterator.hasNext()) {
							ValueType predOut = context.getValueAfter(predecessorIterator.next());
							inValue = meet(inValue, predOut);
						}
						context.setValueBefore(nodeUnit, inValue);
					}
					ValueType lastOut = context.getValueAfter(nodeUnit);
					ValueType inValue = context.getValueBefore(nodeUnit);
					ValueType outValue = function(context, nodeUnit, inValue);

					if (outValue == null)
						outValue = lastOut;
					context.setValueAfter(nodeUnit, outValue);

					if (outValue.equals(lastOut) == false) {
						for (Node successor : context.getCtrlGraph().getSuccsOf(nodeUnit)) {
							context.getAnalyseList().add(successor);
						}
						if (context.getCtrlGraph().getTails().contains(nodeUnit)) {
							context.getAnalyseList().add(null);
						}
					}
				} else {
					assert (context.getAnalyseList().isEmpty());
					ValueType exitFlow = topValue();
					for (Node tail : context.getCtrlGraph().getTails()) {
						ValueType tailOut = context.getValueAfter(tail);
						exitFlow = meet(exitFlow, tailOut);
					}
					context.setExitValue(exitFlow);
					context.makeAnalysed();
					Set<Call<Method,Node,ValueType>> callersSet =  contextTable.getContextCallers(context);
					if (callersSet != null) {
						List<Call<Method,Node,ValueType>> callers = new LinkedList<>(callersSet);
						Collections.sort(callers);
						for (Call<Method,Node,ValueType> call : callers) {
							Context<Method,Node,ValueType> callingContext = call.getCallContext();
							Node callingNode = call.getCallNode();
							callingContext.getAnalyseList().add(callingNode);
							if (!analysisStack.contains(callingContext)) {
								analysisStack.push(callingContext);
							}
						}
					}

					analysisUtil.finishOldAnalyse(context, contextTable, isFreeResults, analyseList);
				}
			} else {
				analysisStack.remove(context);
			}
		}
		analysisUtil.checkIfAllAnalyzed(contexts);
	}

	protected void initContext(Context<Method,Node,ValueType> context, ValueType entryValue) {
		Method method = context.getMethod();

		for (Node unit : context.getCtrlGraph()) {
			context.setValueBefore(unit, topValue());
			context.setValueAfter(unit, topValue());
		}
		context.setEntryValue(copy(entryValue));
		for (Node unit : context.getCtrlGraph().getHeads()) {
			context.setValueBefore(unit, copy(entryValue));
			context.getAnalyseList().add(unit);
		}
		if (!contexts.containsKey(method)) {
			contexts.put(method, new LinkedList<Context<Method,Node,ValueType>>());
		}
		contexts.get(method).add(context);
		analysisStack.add(context);

	}

	protected ValueType processCall(Context<Method,Node,ValueType> callerContext, Node callNode, Method method, ValueType entryValue) {
		Call<Method,Node,ValueType> call = new Call<>(callerContext, callNode);

		Context<Method,Node,ValueType> calleeContext = getContext(method, entryValue);
		if (calleeContext == null) {
			calleeContext = new Context<>(method, representation().getCtrlGraph(method), false);
			initContext(calleeContext, entryValue);
			if (isVerbose) {
				System.out.println("[NEW] X" + callerContext + " -> X" + calleeContext + " " + method + " ");
			}
		}

		contextTable.addTransition(call, calleeContext);

		if (calleeContext.isAnalysed()) {
			if (isVerbose) {
				System.out.println("[MARK] X" + callerContext + " -> X" + calleeContext + " " + method + " ");
			}
			return calleeContext.getExitValue();
		} else {
			return null;
		}
	}
	
	protected abstract ValueType function(Context<Method,Node,ValueType> context, Node nodeUnit, ValueType inValue);

}
