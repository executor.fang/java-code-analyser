package lerhyd;

import soot.toolkits.graph.DirectedGraph;

import java.util.List;

public interface Representation<Method,Node> {

	List<Method> getEntryPoints();
	DirectedGraph<Node> getCtrlGraph(Method method);
	boolean isCall(Node node);
	List<Method> getTargets(Method callerMethod, Node callNode);

}
