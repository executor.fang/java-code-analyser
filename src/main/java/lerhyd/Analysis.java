package lerhyd;


import java.util.*;

public abstract class Analysis<Method,Node,ValueType> {

	protected final NavigableSet<Context<Method,Node,ValueType>> analyseList;
	protected final Map<Method,List<Context<Method,Node,ValueType>>> contexts;
	protected final ContextTable<Method,Node,ValueType> contextTable;
	protected final boolean isReverse;
	protected boolean isFreeResults;
	protected boolean isVerbose;

	public Analysis(boolean isReverse) {
		this.isReverse = isReverse;
		contexts = new HashMap<>();
		contextTable = new ContextTable<>();
		analyseList = new TreeSet<>();
	}

	public abstract ValueType edgeValue(Method entryPoint);

	public abstract ValueType copy(ValueType value);

	public abstract void doAnalysis();

	public List<Context<Method,Node,ValueType>> getContexts(Method method) {
		if (contexts.containsKey(method)) {
			return Collections.unmodifiableList(contexts.get(method));
		} else {
			return Collections.unmodifiableList(new LinkedList<Context<Method,Node,ValueType>>());
		}
	}

	public Context<Method,Node,ValueType> getContext(Method method, ValueType value) {
		if (!contexts.containsKey(method)) {
			return null;
		}
		if (isReverse) {
			for (Context<Method,Node,ValueType> context : contexts.get(method)) {
				if (value.equals(context.getExitValue())) {
					return context;
				}
			}
		} else {
			for (Context<Method,Node,ValueType> context : contexts.get(method)) {
				if (value.equals(context.getEntryValue())) {
					return context;
				}
			}
		}
		return null;
	}

	public Solution<Node,ValueType> getMeetSolution() {
		Map<Node,ValueType> inValues = new HashMap<>();
		Map<Node,ValueType> outValues = new HashMap<>();
		for (Method method : contexts.keySet()) {
			for (Node node : representation().getCtrlGraph(method)) {
				ValueType inValue = topValue();
				ValueType outValue = topValue();
				for (Context<Method,Node,ValueType> context : contexts.get(method)) {
					inValue = meet(inValue, context.getValueBefore(node));
					outValue = meet(outValue, context.getValueAfter(node));
				}
				inValues.put(node, inValue);
				outValues.put(node, outValue);
			}
		}
		return new Solution<>(inValues, outValues);
	}

	public ContextTable<Method,Node,ValueType> getContextTable() {
		return contextTable;
	}

	public Set<Method> getMethods() {
		return Collections.unmodifiableSet(contexts.keySet());
	}

	public abstract ValueType meet(ValueType firstValue, ValueType secondValue);

	public abstract Representation<Method,Node> representation();

	public abstract ValueType topValue();

    public abstract ValueType normalFunc(Context<Method, Node, ValueType> context, Node node, ValueType inValue);

    public abstract ValueType callEntryFunc(Context<Method,Node,ValueType> context, Method method, Node node, ValueType inValue);

    public abstract ValueType callExitFunc(Context<Method,Node,ValueType> context, Method method, Node node, ValueType exitValue);

    public abstract ValueType callLocalFunc(Context<Method,Node,ValueType> context, Node node, ValueType inValue);

}
