package lerhyd;

import lerhyd.util.AnalysisUtil;

import java.util.LinkedList;
import java.util.List;

public abstract class ForwardAnalysis<Method,Node,ValueType> extends Analysis<Method,Node,ValueType> {

    public AnalysisUtil analysisUtil;

	public ForwardAnalysis() {
		super(false);
		analysisUtil = new AnalysisUtil();
	}

	@Override
	public void doAnalysis() {

		for (Method method : representation().getEntryPoints()) {
			initContext(method, edgeValue(method));
		}

		while (!analyseList.isEmpty()) {
			Context<Method,Node,ValueType> currentContext = analyseList.last();
			if (currentContext.getAnalyseList().isEmpty()) {
				currentContext.makeAnalysed();
				analyseList.remove(currentContext);
				continue;
			}
			Node node = currentContext.getAnalyseList().pollFirst();

			if (node != null) {
				List<Node> predecessors = currentContext.getCtrlGraph().getPredsOf(node);
				if (predecessors.size() != 0) {
					ValueType inValue = topValue();
					for (Node predecessor : predecessors) {
						ValueType predecessorOut = currentContext.getValueAfter(predecessor);
						inValue = meet(inValue, predecessorOut);
					}
					currentContext.setValueBefore(node, inValue);
				}
				ValueType lastOut = currentContext.getValueAfter(node);
				ValueType in = currentContext.getValueBefore(node);

				if (isVerbose) {
					System.out.println("IN = " + in);
					System.err.println(node);
				}
				ValueType outValue;
				if (representation().isCall(node)) {
					outValue = topValue();
					boolean isMarked = false;
					if (!representation().getTargets(currentContext.getMethod(), node).isEmpty()) {
					for (Method targetMethod : representation().getTargets(currentContext.getMethod(), node)) {
						ValueType entryValue = callEntryFunc(currentContext, targetMethod, node, in);

						Call<Method,Node,ValueType> call = new Call<>(currentContext, node);
						Context<Method,Node,ValueType> targetContext = getContext(targetMethod, entryValue);
						if (targetContext == null) {
							targetContext = initContext(targetMethod, entryValue);
							if (isVerbose) {
								System.out.println("[NEW] X" + currentContext + " -> X" + targetContext + " " + targetMethod + " ");
								System.out.println("ENTRY(X" + targetContext + ") = " + entryValue);
							}

						}
						contextTable.addTransition(call, targetContext);

						if (targetContext.isAnalysed()) {
							isMarked = true;
							ValueType exitValue = targetContext.getExitValue();
							if (isVerbose) {
								System.out.println("[MARK] X" + currentContext + " -> X" + targetContext + " " + targetMethod + " ");
									System.out.println("EXIT(X" + targetContext + ") = " + exitValue);
							}
							ValueType returnedValue = callExitFunc(currentContext, targetMethod, node, exitValue);
							outValue = meet(outValue, returnedValue);
						}
					}

					if (isMarked) {
						ValueType localValue = callLocalFunc(currentContext, node, in);
						outValue = meet(outValue, localValue);
						}
						else {
							outValue = callLocalFunc(currentContext, node, in);
						}
					}
					else
					{
						outValue = callLocalFunc(currentContext, node, in);
					}
				} else {
					outValue = normalFunc(currentContext, node, in);
				}
				if (isVerbose) {
					System.out.println("OUT = " + outValue);
					System.out.println("*********************************************");
				}

				outValue = meet(outValue, lastOut);
				currentContext.setValueAfter(node, outValue);

				if (outValue.equals(lastOut) == false) {
					for (Node successor : currentContext.getCtrlGraph().getSuccsOf(node)) {
						currentContext.getAnalyseList().add(successor);
					}
				}
				if (currentContext.getCtrlGraph().getTails().contains(node)) {
					currentContext.getAnalyseList().add(null);
				}
			} else {
				assert (currentContext.getAnalyseList().isEmpty());
				ValueType exitValue = topValue();
				for (Node tailNode : currentContext.getCtrlGraph().getTails()) {
					ValueType tailOut = currentContext.getValueAfter(tailNode);
					exitValue = meet(exitValue, tailOut);
				}
				currentContext.setExitValue(exitValue);
				analysisUtil.finishAnalyse(currentContext, contextTable, isFreeResults, analyseList);
			}
		}
		analysisUtil.checkIfAllAnalyzed(contexts);
	}

	protected Context<Method, Node, ValueType> initContextWithMethod(Method method, ValueType entryValue) {
		Context<Method, Node, ValueType> context = new Context<>(method);
		context.setEntryValue(entryValue);
		context.setExitValue(copy(entryValue));
		context.makeAnalysed();
		return context;
	}

	protected Context<Method,Node,ValueType> initContext(Method method, ValueType entryValue) {
		Context<Method,Node,ValueType> context = new Context<>(method, representation().getCtrlGraph(method), false);
		for (Node unit : context.getCtrlGraph()) {
			context.setValueBefore(unit, topValue());
			context.setValueAfter(unit, topValue());
			context.getAnalyseList().add(unit);
		}
		context.setEntryValue(copy(entryValue));
		for (Node unit : context.getCtrlGraph().getHeads()) {
			context.setValueBefore(unit, copy(entryValue));
		}
		context.setExitValue(topValue());
		if (!contexts.containsKey(method)) {
			contexts.put(method, new LinkedList<Context<Method,Node,ValueType>>());
		}
		contexts.get(method).add(context);
		analyseList.add(context);
		return context;

	}

}

