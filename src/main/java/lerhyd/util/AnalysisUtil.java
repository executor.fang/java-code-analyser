package lerhyd.util;

import lerhyd.Call;
import lerhyd.Context;
import lerhyd.ContextTable;

import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;

public class AnalysisUtil<Method, Node, ValueType> {

    public void finishAnalyse(Context currentContext, ContextTable contextTable, boolean isFreeResults, NavigableSet analyseList){
        currentContext.makeAnalysed();
        Set<Call<Method,Node,ValueType>> callers =  contextTable.getContextCallers(currentContext);
        if (callers != null) {
            for (Call<Method,Node,ValueType> call : callers) {
                Context<Method,Node,ValueType> callingContext = call.getCallContext();
                Node callNode = call.getCallNode();
                callingContext.getAnalyseList().add(callNode);
                analyseList.add(callingContext);
            }
        }
        finishOldAnalyse(currentContext, contextTable, isFreeResults, analyseList);
    }

    public void finishOldAnalyse(Context currentContext, ContextTable contextTable, boolean isFreeResults, NavigableSet analyseList){
        if (isFreeResults) {
            Set<Context<Method,Node,ValueType>> reachableContexts = contextTable.getReachableSet(currentContext, true);
            boolean canFree = true;
            for (Context<Method,Node,ValueType> reachableContext : reachableContexts) {
                if (analyseList.contains(reachableContext)) {
                    canFree = false;
                    break;
                }
            }
            if (canFree) {
                for (Context<Method,Node,ValueType> reachableContext : reachableContexts) {
                    reachableContext.free();
                }
            }
        }
    }

    public void checkIfAllAnalyzed(Map<Method,List<Context<Method,Node,ValueType>>> contexts){
        for (List<Context<Method,Node,ValueType>> contextList : contexts.values()) {
            for (Context<Method,Node,ValueType> context : contextList) {
                if (context.isAnalysed() == false) {
                    System.err.println("This context is not analysed! " + context +
                            " " + context.getMethod());
                }
            }
        }
    }

}
