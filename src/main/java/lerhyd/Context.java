package lerhyd;

import soot.toolkits.graph.DirectedGraph;
import soot.toolkits.graph.SlowPseudoTopologicalOrderer;

import java.util.*;

public class Context<Method,Node,ValueType> implements soot.Context, Comparable<Context<Method,Node,ValueType>> {

	private static int count = 0;

	static java.util.Set<Object> freeContexts = new java.util.HashSet<>();
	static int totalNodes = 0;
	static int workNodes = 0;

	private int id;
	private boolean isAnalysed;
	private DirectedGraph<Node> ctrlGraph;
	private ValueType entryValue;
	private ValueType exitValue;
	private Method method;
	private Map<Node,ValueType> outValues;
	private Map<Node,ValueType> inValues;
	private NavigableSet<Node> analyseList;

	public Context(Method method) {
		count++;
		this.id = count;
		this.method = method;
		this.inValues = new HashMap<>();
		this.outValues = new HashMap<>();
		this.isAnalysed = false;
		this.analyseList = new TreeSet<>();
	}

	public Context(Method method, DirectedGraph<Node> g, boolean isReverse) {
		count++;
		this.id = count;
		this.method = method;
		this.ctrlGraph = g;
		this.inValues = new HashMap<>();
		this.outValues = new HashMap<>();
		this.isAnalysed = false;
		totalNodes = totalNodes + ctrlGraph.size();
		workNodes = workNodes + ctrlGraph.size();

		@SuppressWarnings("unchecked")
		List<Node> orderedNodes = new SlowPseudoTopologicalOrderer().newList(ctrlGraph, isReverse);

		final Map<Node,Integer> numbers = new HashMap<>();
		int number = 1;
		for (Node node : orderedNodes) {
			numbers.put(node, number);
			number++;
		}
		numbers.put(null, Integer.MAX_VALUE);

		this.analyseList = new TreeSet<>(new Comparator<Node>() {
			@Override
			public int compare(Node firstNode, Node secondNode) {
				return numbers.get(firstNode) - numbers.get(secondNode);
			}
		});
	}

	@Override
	public int compareTo(Context<Method,Node,ValueType> context) {
		return this.getId() - context.getId();
	}

	public void free() {
		workNodes = workNodes - ctrlGraph.size();
		inValues = null;
		outValues = null;
		ctrlGraph = null;
		analyseList = null;
		freeContexts.add(this);
	}

	public DirectedGraph<Node> getCtrlGraph() {
		return ctrlGraph;
	}

	public ValueType getEntryValue() {
		return entryValue;
	}

	public ValueType getExitValue() {
		return exitValue;
	}

	public int getId() {
		return id;
	}

	public Method getMethod() {
		return method;
	}

	public ValueType getValueAfter(Node node) {
		return outValues.get(node);
	}

	public ValueType getValueBefore(Node node) {
		return inValues.get(node);
	}

	public NavigableSet<Node> getAnalyseList() {
		return analyseList;
	}

	public boolean isAnalysed() {
		return isAnalysed;
	}

	boolean isFreed() {
		return inValues == null && outValues == null;
	}

	public void makeAnalysed() {
		this.isAnalysed = true;
	}

	public void setEntryValue(ValueType entryValue) {
		this.entryValue = entryValue;
	}

	public void setExitValue(ValueType exitValue) {
		this.exitValue = exitValue;
	}

	public void setValueAfter(Node node, ValueType value) {
		outValues.put(node, value);
	}

	public void setValueBefore(Node node, ValueType value) {
		inValues.put(node, value);
	}

	@Override
	public String toString() {
		return Integer.toString(id);
	}
}
