package lerhyd.soot;

import lerhyd.Representation;
import soot.Scene;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.toolkits.callgraph.Edge;
import soot.toolkits.graph.DirectedGraph;
import soot.toolkits.graph.ExceptionalUnitGraph;

import java.util.*;

public class DefaultJRepresentation implements Representation<SootMethod, Unit> {
	private Map<SootMethod, DirectedGraph<Unit>> graphCache;
	private DefaultJRepresentation() {
        graphCache = new HashMap<>();
	}

	@Override
	public List<SootMethod> getEntryPoints() {
		return Collections.singletonList(Scene.v().getMainMethod());
	}

	@Override
	public DirectedGraph<Unit> getCtrlGraph(SootMethod method) {
		if (graphCache.containsKey(method) == false) {
            graphCache.put(method, new ExceptionalUnitGraph(method.getActiveBody()));
		}
		return graphCache.get(method);
	}

	@Override
	public boolean isCall(Unit nodeUnit) {
		return ((soot.jimple.Stmt) nodeUnit).containsInvokeExpr();
	}

	@Override
	public List<SootMethod> getTargets(SootMethod method, Unit nodeUnit) {
		List<SootMethod> targets = new LinkedList<>();
		Iterator<Edge> iterator = Scene.v().getCallGraph().edgesOutOf(nodeUnit);
		while(iterator.hasNext()) {
			Edge edge = iterator.next();
			if (edge.isExplicit()) {
				targets.add(edge.tgt());
			}
		}
		return targets;
	}

	private static DefaultJRepresentation singleton = new DefaultJRepresentation();

	public static DefaultJRepresentation v() { return singleton; }
	
}
