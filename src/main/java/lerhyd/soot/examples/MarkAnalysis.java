package lerhyd.soot.examples;

import lerhyd.Context;
import lerhyd.ForwardAnalysis;
import lerhyd.Representation;
import lerhyd.soot.DefaultJRepresentation;
import lerhyd.soot.examples.sign.Sign;
import soot.*;
import soot.jimple.*;
import soot.jimple.internal.AbstractNegExpr;
import soot.jimple.internal.JimpleLocal;

import java.util.HashMap;
import java.util.Map;

public class MarkAnalysis extends ForwardAnalysis<SootMethod, Unit, Map<Local, Sign>> {

	private static final Local RETURN = new JimpleLocal("@return", IntType.v());

	public MarkAnalysis() {
		super();
		isVerbose = true;
	}

	private Sign defineSign(Value valueToDefine, Map<Local, Sign> dataMap) {
		if (valueToDefine instanceof Local) {
			Local loc = (Local) valueToDefine;
			if (dataMap.containsKey(loc)) {
				return dataMap.get(loc);
			} else {
				return Sign.TOP;
			}
		} else if (valueToDefine instanceof NumericConstant) {
			NumericConstant number = (NumericConstant) valueToDefine;
			NumericConstant zero = IntConstant.v(0);
			NumericConstant one = IntConstant.v(1);
			if (number.lessThan(zero).equals(one)) {
				return Sign.NEGATIVE;
			} else if (number.greaterThan(zero).equals(one)) {
				return Sign.POSITIVE;
			} else {
				return Sign.ZERO;
			}
		} else if (valueToDefine instanceof BinopExpr) {
			BinopExpr expression = (BinopExpr) valueToDefine;
			Value firstValue = expression.getOp1();
			Value secondValue = expression.getOp2();
			Sign firstSign = defineSign(firstValue, dataMap);
			Sign secondSign = defineSign(secondValue, dataMap);
			if (valueToDefine instanceof AddExpr) {
				return firstSign.plus(secondSign);
			} else if (valueToDefine instanceof MulExpr) {
				return firstSign.multiply(secondSign);
			} else {
				return Sign.BOTTOM;
			}
		} else if (valueToDefine instanceof UnopExpr) {
			if (valueToDefine instanceof AbstractNegExpr) {
				Value operand = ((AbstractNegExpr) valueToDefine).getOp();
				Sign sign = defineSign(operand, dataMap);
				return sign.negate();
			} else {
				return Sign.BOTTOM;
			}
		} else {
			return Sign.BOTTOM;
		}
	}

	private void assign(Local leftStatementLoc, Value rightStatementValue, Map<Local, Sign> input, Map<Local, Sign> output) {
		if (leftStatementLoc.getType() instanceof IntType) {
			if (rightStatementValue instanceof CastExpr) {
                rightStatementValue = ((CastExpr) rightStatementValue).getOp();
			}
			Sign sign = defineSign(rightStatementValue, input);
			output.put(leftStatementLoc, sign);
		}
	}

	@Override
	public Map<Local, Sign> normalFunc(
			Context<SootMethod, Unit, Map<Local, Sign>> context, Unit unit,
			Map<Local, Sign> inValue) {
		Map<Local, Sign> outValue = copy(inValue);
		if (unit instanceof AssignStmt) {
			Value leftOperand = ((AssignStmt) unit).getLeftOp();
			Value rightOperand = ((AssignStmt) unit).getRightOp();
			if (leftOperand instanceof Local) {
				assign((Local) leftOperand, rightOperand, inValue, outValue);
			}
		} else if (unit instanceof ReturnStmt) {
			Value rightOperand = ((ReturnStmt) unit).getOp();
			assign(RETURN, rightOperand, inValue, outValue);
		}
		return outValue;
	}

	@Override
	public Map<Local, Sign> callEntryFunc(
			Context<SootMethod, Unit, Map<Local, Sign>> context, SootMethod calledMethod, Unit unit,
			Map<Local, Sign> inValue) {
		Map<Local, Sign> entryValue = topValue();
		InvokeExpr invokeExpr = ((Stmt) unit).getInvokeExpr();
		for (int i = 0; i < invokeExpr.getArgCount(); i++) {
			Value argumentValue = invokeExpr.getArg(i);
			Local parameterLoc = calledMethod.getActiveBody().getParameterLocal(i);
			assign(parameterLoc, argumentValue, inValue, entryValue);
		}
		if (invokeExpr instanceof InstanceInvokeExpr) {
			Value instanceValue = ((InstanceInvokeExpr) invokeExpr).getBase();
			Local thisLocal = calledMethod.getActiveBody().getThisLocal();
			assign(thisLocal, instanceValue, inValue, entryValue);
		}
		return entryValue;
	}
	
	@Override
	public Map<Local, Sign> callExitFunc(Context<SootMethod, Unit, Map<Local, Sign>> context, SootMethod calledMethod, Unit unit, Map<Local, Sign> exitValue) {
		Map<Local, Sign> afterCallValue = topValue();
		if (unit instanceof AssignStmt) {
			Value leftOperand = ((AssignStmt) unit).getLeftOp();
			assign((Local) leftOperand, RETURN, exitValue, afterCallValue);
		}
		return afterCallValue;
	}

	@Override
	public Map<Local, Sign> callLocalFunc(Context<SootMethod, Unit, Map<Local, Sign>> context, Unit unit, Map<Local, Sign> inValue) {
		Map<Local, Sign> afterCallValue = copy(inValue);
		if (unit instanceof AssignStmt) {
			Value leftOperand = ((AssignStmt) unit).getLeftOp();
			afterCallValue.remove(leftOperand);
		}
		return afterCallValue;
		
	}
	
	@Override
	public Map<Local, Sign> edgeValue(SootMethod method) {
		return topValue();
	}

	@Override
	public Map<Local, Sign> copy(Map<Local, Sign> source) {
		return new HashMap<>(source);
	}



	@Override
	public Map<Local, Sign> meet(Map<Local, Sign> firstOperand, Map<Local, Sign> secondOperand) {
		Map<Local, Sign> result;
		result = new HashMap<>(firstOperand);
		for (Local x : secondOperand.keySet()) {
			if (firstOperand.containsKey(x)) {
				Sign firstSign = firstOperand.get(x);
				Sign secondSign = secondOperand.get(x);
				result.put(x, firstSign.meet(secondSign));
			} else {
				result.put(x, secondOperand.get(x));
			}
		}
		return result;
	}

	@Override
	public Map<Local, Sign> topValue() {
		return new HashMap<>();
	}

	@Override
	public Representation<SootMethod, Unit> representation() {
		return DefaultJRepresentation.v();
	}

}




