package lerhyd.soot.examples.sign;

public interface Sign {
    Sign meet(Sign sign);
    Sign plus(Sign sign);
    Sign multiply(Sign sign);
    Sign negate();
    Sign BOTTOM = new Sign() {
        @Override
        public Sign meet(Sign sign) {
            return BOTTOM;
        }
        @Override
        public Sign plus(Sign sign) {
            return BOTTOM;
        }
        @Override
        public Sign multiply(Sign sign) {
            return BOTTOM;
        }
        @Override
        public Sign negate() {
            return BOTTOM;
        }
        @Override
        public String toString() {
            return "_|_";
        }
    };

    Sign TOP = new Sign() {
        @Override
        public Sign meet(Sign sign) {
            return sign;
        }
        @Override
        public Sign plus(Sign sign) {
            return sign;
        }
        @Override
        public Sign multiply(Sign sign) {
            return sign;
        }
        @Override
        public Sign negate() {
            return TOP;
        }
        @Override
        public String toString() {
            return "T";
        }
    };
    Sign ZERO = new Sign() {
        @Override
        public Sign meet(Sign sign) {
            if (sign == TOP || sign == ZERO) {
                return ZERO;
            } else {
                return BOTTOM;
            }
        }
        @Override
        public Sign plus(Sign sign) {
            if (sign.equals(TOP) || sign.equals(ZERO)) {
                return ZERO;
            } else if (sign.equals(POSITIVE)) {
                return POSITIVE;
            } else if (sign.equals(NEGATIVE)) {
                return NEGATIVE;
            } else {
                return BOTTOM;
            }
        }
        @Override
        public Sign multiply(Sign sign) {
            if (sign.equals(TOP) || sign.equals(ZERO) || sign.equals(POSITIVE) || sign.equals(NEGATIVE)) {
                return ZERO;
            } else {
                return BOTTOM;
            }
        }
        @Override
        public Sign negate() {
            return ZERO;
        }
        @Override
        public String toString() {
            return "0";
        }
    };

    Sign POSITIVE = new Sign() {
        @Override
        public Sign meet(Sign sign) {
            if (sign.equals(TOP) || sign.equals(POSITIVE)) {
                return POSITIVE;
            } else {
                return BOTTOM;
            }
        }
        @Override
        public Sign plus(Sign sign) {
            if (sign.equals(TOP) || sign.equals(POSITIVE) || sign.equals(ZERO)) {
                return POSITIVE;
            } else if (sign.equals(NEGATIVE)) {
                return BOTTOM;
            } else {
                return BOTTOM;
            }
        }
        @Override
        public Sign multiply(Sign sign) {
            if (sign.equals(TOP) || sign.equals(POSITIVE)) {
                return POSITIVE;
            } else if (sign.equals(ZERO)) {
                return ZERO;
            } else if (sign.equals(NEGATIVE)) {
                return NEGATIVE;
            } else {
                return BOTTOM;
            }
        }
        @Override
        public Sign negate() {
            return NEGATIVE;
        }
        @Override
        public String toString() {
            return "+";
        }
    };

    Sign NEGATIVE = new Sign() {
        @Override
        public Sign meet(Sign sign) {
            if (sign.equals(TOP) || sign.equals(NEGATIVE)) {
                return NEGATIVE;
            } else {
                return BOTTOM;
            }
        }
        @Override
        public Sign plus(Sign sign) {
            if (sign.equals(TOP) || sign.equals(NEGATIVE) || sign.equals(ZERO)) {
                return NEGATIVE;
            } else if (sign == POSITIVE) {
                return BOTTOM;
            } else {
                return BOTTOM;
            }
        }
        @Override
        public Sign multiply(Sign sign) {
            if (sign.equals(TOP) || sign.equals(NEGATIVE)) {
                return POSITIVE;
            } else if (sign == ZERO) {
                return ZERO;
            } else if (sign == POSITIVE) {
                return NEGATIVE;
            } else {
                return BOTTOM;
            }
        }
        @Override
        public Sign negate() {
            return POSITIVE;
        }
        @Override
        public String toString() {
            return "-";
        }
    };
}