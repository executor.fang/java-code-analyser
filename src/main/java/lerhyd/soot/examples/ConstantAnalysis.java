package lerhyd.soot.examples;

import lerhyd.Context;
import lerhyd.ForwardAnalysis;
import lerhyd.Representation;
import lerhyd.soot.DefaultJRepresentation;
import soot.Local;
import soot.SootMethod;
import soot.Unit;
import soot.Value;
import soot.jimple.*;
import soot.jimple.internal.JimpleLocal;

import java.util.HashMap;
import java.util.Map;

public class ConstantAnalysis extends ForwardAnalysis<SootMethod, Unit, Map<Local, Constant>> {

	private static final Local RETURN = new JimpleLocal("@return", null);

	public ConstantAnalysis() {
		super();
		isVerbose = true;
	}

	private void assign(Local leftStatement, Value rightStatement, Map<Local, Constant> input, Map<Local, Constant> output) {
		if (rightStatement instanceof CastExpr) {
            rightStatement = ((CastExpr) rightStatement).getOp();
		}
		if (rightStatement instanceof Constant) {
			output.put(leftStatement, (Constant) rightStatement);
		} else if (rightStatement instanceof Local) {
			if(input.containsKey(rightStatement)) {
				output.put(leftStatement, input.get(rightStatement));
			}
		} else {
			output.put(leftStatement, null);
		}			
	}

	@Override
	public Map<Local, Constant> normalFunc(Context<SootMethod, Unit, Map<Local, Constant>> context, Unit unit,
			Map<Local, Constant> inValue) {
		Map<Local, Constant> outValue = copy(inValue);
		if (unit instanceof AssignStmt) {
			Value leftOperand = ((AssignStmt) unit).getLeftOp();
			Value rightOperand = ((AssignStmt) unit).getRightOp();
			if (leftOperand instanceof Local) {
				assign((Local) leftOperand, rightOperand, inValue, outValue);
			}
		} else if (unit instanceof ReturnStmt) {
			Value rightOperand = ((ReturnStmt) unit).getOp();
			assign(RETURN, rightOperand, inValue, outValue);
		}
		return outValue;
	}

	@Override
	public Map<Local, Constant> callEntryFunc(Context<SootMethod, Unit, Map<Local, Constant>> context, SootMethod calledMethod, Unit unit, Map<Local, Constant> inValue) {
		Map<Local, Constant> entryValue = topValue();
		InvokeExpr invokeExpr = ((Stmt) unit).getInvokeExpr();
		for (int i = 0; i < invokeExpr.getArgCount(); i++) {
			Value arg = invokeExpr.getArg(i);
			Local param = calledMethod.getActiveBody().getParameterLocal(i);
			assign(param, arg, inValue, entryValue);
		}
		if (invokeExpr instanceof InstanceInvokeExpr) {
			Value instanceValue = ((InstanceInvokeExpr) invokeExpr).getBase();
			Local thisLocal = calledMethod.getActiveBody().getThisLocal();
			assign(thisLocal, instanceValue, inValue, entryValue);
		}
		return entryValue;
	}
	
	@Override
	public Map<Local, Constant> callExitFunc(Context<SootMethod, Unit, Map<Local, Constant>> context, SootMethod calledMethod, Unit unit, Map<Local, Constant> exitValue) {
		Map<Local, Constant> afterCallValue = topValue();
		if (unit instanceof AssignStmt) {
			Value leftOperand = ((AssignStmt) unit).getLeftOp();
			assign((Local) leftOperand, RETURN, exitValue, afterCallValue);
		}
		return afterCallValue;
	}

	@Override
	public Map<Local, Constant> callLocalFunc(Context<SootMethod, Unit, Map<Local, Constant>> context, Unit unit, Map<Local, Constant> inValue) {
		Map<Local, Constant> afterCallValue = copy(inValue);
		if (unit instanceof AssignStmt) {
			Value leftOperand = ((AssignStmt) unit).getLeftOp();
			afterCallValue.remove(leftOperand);
		}
		return afterCallValue;
		
	}
	
	@Override
	public Map<Local, Constant> edgeValue(SootMethod method) {
		return topValue();
	}

	@Override
	public Map<Local, Constant> copy(Map<Local, Constant> source) {
		return new HashMap<>(source);
	}



	@Override
	public Map<Local, Constant> meet(Map<Local, Constant> firstOperand, Map<Local, Constant> secondOperand) {
		Map<Local, Constant> result;
		result = new HashMap<>(firstOperand);
		for (Local localValue : secondOperand.keySet()) {
			if (firstOperand.containsKey(localValue)) {
				Constant firstConstant = firstOperand.get(localValue);
				Constant secondConstant = secondOperand.get(localValue);
				if (firstConstant != null && firstConstant.equals(secondConstant) == false) {
					result.put(localValue, null);
				}
			} else {
				result.put(localValue, secondOperand.get(localValue));
			}
		}
		return result;
	}

	@Override
	public Map<Local, Constant> topValue() {
		return new HashMap<>();
	}

	@Override
	public Representation<SootMethod, Unit> representation() {
		return DefaultJRepresentation.v();
	}

}
