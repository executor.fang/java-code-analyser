package lerhyd;

public class Call<Method,Node,ValueType> implements Comparable<Call<Method,Node,ValueType>> {

	private final Context<Method,Node,ValueType> callContext;
	private final Node callNode;

	public Call(Context<Method,Node,ValueType> callContext, Node callNode) {
		this.callContext = callContext;
		this.callNode = callNode;
	}

	@Override
	public int compareTo(Call<Method,Node,ValueType> otherCall) {
		return this.callContext.getId() - otherCall.callContext.getId();
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Call other = (Call) obj;
		if (callNode == null) {
			if (other.callNode != null)
				return false;
		} else if (!callNode.equals(other.callNode))
			return false;
		if (callContext == null) {
			if (other.callContext != null)
				return false;
		} else if (!callContext.equals(other.callContext))
			return false;
		return true;
	}

	public Context<Method,Node,ValueType> getCallContext() {
		return callContext;
	}

	public Node getCallNode() {
		return callNode;
	}

	@Override
	public int hashCode() {
		final int prime = 666;
		int result = 1;
		result = prime * result + ((callNode == null) ? 0 : callNode.hashCode());
		result = prime * result + ((callContext == null) ? 0 : callContext.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "X"+callContext+": "+callNode;
	}
}
