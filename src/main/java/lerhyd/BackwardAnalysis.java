package lerhyd;

import lerhyd.util.AnalysisUtil;

import java.util.LinkedList;
import java.util.List;

public abstract class BackwardAnalysis<Method,Node,ValueType> extends Analysis<Method,Node,ValueType> {

    private AnalysisUtil analysisUtil;

	public BackwardAnalysis() {
		super(true);
		analysisUtil = new AnalysisUtil();
	}

	@Override
	public void doAnalysis() {
		for (Method method : representation().getEntryPoints()) {
			initContext(method, edgeValue(method));
		}

		while (!analyseList.isEmpty()) {
			Context<Method,Node,ValueType> currentContext = analyseList.last();

			if (currentContext.getAnalyseList().isEmpty()) {
				analyseList.remove(currentContext);
				continue;
			}

			Node node = currentContext.getAnalyseList().pollFirst();
			if (node != null) {
				List<Node> successors = currentContext.getCtrlGraph().getSuccsOf(node);
				if (successors.size() != 0) {
					ValueType out = topValue();
					for (Node successor : successors) {
						ValueType successorIn = currentContext.getValueBefore(successor);
						out = meet(out, successorIn);
					}
					currentContext.setValueAfter(node, out);
				}
				ValueType lastIn = currentContext.getValueBefore(node);
				ValueType outValue = currentContext.getValueAfter(node);
				ValueType inValue;

				if (representation().isCall(node)) {
					inValue = topValue();
					boolean isMark = false;
					for (Method targetMethod : representation().getTargets(currentContext.getMethod(), node)) {
						ValueType exitValue = callExitFunc(currentContext, targetMethod, node, outValue);
						
						Call<Method,Node,ValueType> call = new Call<>(currentContext, node);
						Context<Method,Node,ValueType> targetContext = getContext(targetMethod, exitValue);
						if (targetContext == null) {
							targetContext = initContext(targetMethod, exitValue);
							if (isVerbose) {
								System.out.println("[NEW] X" + currentContext + " -> X" + targetContext + " " + targetMethod + " ");
							}
						}
						contextTable.addTransition(call, targetContext);
						if (targetContext.isAnalysed()) {
							isMark = true;
							if (isVerbose) {
								System.out.println("[MARK] X" + currentContext + " -> X" + targetContext + " " + targetMethod + " ");
							}
							ValueType entryValue = targetContext.getEntryValue();
							ValueType callValue = callEntryFunc(currentContext, targetMethod, node, entryValue);
							inValue = meet(inValue, callValue);
						} 
					}
					if (isMark) {
						ValueType localValue = callLocalFunc(currentContext, node, outValue);
						inValue = meet(inValue, localValue);
					}
				} else {
					inValue = normalFunc(currentContext, node, outValue);
				}
				inValue = meet(inValue, lastIn);
				currentContext.setValueBefore(node, inValue);
				if (inValue.equals(lastIn) == false) {
					for (Node predecessors : currentContext.getCtrlGraph().getPredsOf(node)) {
						currentContext.getAnalyseList().add(predecessors);
					}
				}
				if (currentContext.getCtrlGraph().getHeads().contains(node)) {
					currentContext.getAnalyseList().add(null);
				}
			} else {
				assert (currentContext.getAnalyseList().isEmpty());
				ValueType entryValue = topValue();
				for (Node headNode : currentContext.getCtrlGraph().getHeads()) {
					ValueType headIn = currentContext.getValueBefore(headNode);
					entryValue = meet(entryValue, headIn);
				}
				currentContext.setEntryValue(entryValue);
				analysisUtil.finishAnalyse(currentContext, contextTable, isFreeResults, analyseList);
			}
			
		}
		analysisUtil.checkIfAllAnalyzed(contexts);
	}

	protected Context<Method,Node,ValueType> initContext(Method method, ValueType exitValue) {
		Context<Method,Node,ValueType> context = new Context<>(method, representation().getCtrlGraph(method), true);
		for (Node unit : context.getCtrlGraph()) {
			context.setValueBefore(unit, topValue());
			context.setValueAfter(unit, topValue());
			context.getAnalyseList().add(unit);
		}
		context.setExitValue(copy(exitValue));
		for (Node unit : context.getCtrlGraph().getTails()) {
			context.setValueAfter(unit, copy(exitValue));
		}
		context.setEntryValue(topValue());
		if (!contexts.containsKey(method)) {
			contexts.put(method, new LinkedList<Context<Method,Node,ValueType>>());
		}
		contexts.get(method).add(context);
		analyseList.add(context);
		
		return context;
	}

}
