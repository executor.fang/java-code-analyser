package lerhyd;

import java.util.*;

public class ContextTable<Method,Node,ValueType> {

	protected Map<Context<Method,Node,ValueType>,Set<Call<Method,Node,ValueType>>> contextCallers;
	protected Map<Call<Method,Node,ValueType>,Map<Method,Context<Method,Node,ValueType>>> transitions;
	protected Map<Context<Method,Node,ValueType>,Set<Call<Method,Node,ValueType>>> callsOfContexts;
	protected Set<Call<Method,Node,ValueType>> calls;

	public ContextTable() {
		transitions = new HashMap<>();
		contextCallers = new HashMap<>();
		callsOfContexts = new HashMap<>();
		calls = new HashSet<>();
	}

	public void addTransition(Call<Method,Node,ValueType> call, Context<Method,Node,ValueType> context) {
		
		if (context != null) {
			Method targetMethod = context.getMethod();

			if (!contextCallers.containsKey(context)) {
				contextCallers.put(context, new HashSet<Call<Method,Node,ValueType>>());
			}

			if (transitions.containsKey(call) && transitions.get(call).containsKey(targetMethod)) {
				Context<Method,Node,ValueType> oldTarget = transitions.get(call).get(targetMethod);
				contextCallers.get(oldTarget).remove(call);
			}

			if (!transitions.containsKey(call)) {
				transitions.put(call, new HashMap<Method,Context<Method,Node,ValueType>>());
			}
			transitions.get(call).put(targetMethod, context);
			contextCallers.get(context).add(call);
		} else {
			if (transitions.containsKey(call) && transitions.get(call) != null) {
				for (Method method : transitions.get(call).keySet()) {
					Context<Method,Node,ValueType> oldTarget = transitions.get(call).get(method);
					contextCallers.get(oldTarget).remove(call);
				}
			}
			calls.add(call);
		}

		Context<Method,Node,ValueType> source = call.getCallContext();
		if (callsOfContexts.containsKey(source) == false) {
			callsOfContexts.put(source, new HashSet<Call<Method,Node,ValueType>>());
		}
		callsOfContexts.get(source).add(call);
	}

	public Set<Call<Method,Node,ValueType>> getContextCallers(Context<Method,Node,ValueType> context) {
		return contextCallers.get(context);
	}

	public Map<Context<Method,Node,ValueType>,Set<Call<Method,Node,ValueType>>> getCallsOfContexts() {
		return Collections.unmodifiableMap(callsOfContexts);
	}

	public Map<Call<Method,Node,ValueType>,Map<Method,Context<Method,Node,ValueType>>> getTransitions() {
		return Collections.unmodifiableMap(this.transitions);
	}

	public Set<Context<Method,Node,ValueType>> getReachableSet(Context<Method,Node,ValueType> context, boolean isIgnoreFree) {
		Set<Context<Method,Node,ValueType>> reachableContexts = new HashSet<>();

		Stack<Context<Method,Node,ValueType>> stack = new Stack<>();
		stack.push(context);

		while (stack.isEmpty() == false) {
			context = stack.pop();
			if (callsOfContexts.containsKey(context)) {
				for (Call<Method,Node,ValueType> call : callsOfContexts.get(context)) {
					if (calls.contains(call)) {
						continue;
					}
					for (Method method : transitions.get(call).keySet()) {
						Context<Method,Node,ValueType> target = transitions.get(call).get(method);
						if (reachableContexts.contains(target) == false) {
							if (isIgnoreFree && target.isFreed()) {
								continue;
							}
							reachableContexts.add(target);
							stack.push(target);
						}
					}
				}
			}
			
		}		
		return reachableContexts;
	}

	public Set<Call<Method,Node,ValueType>> getCalls() {
		return calls;
	}

}
