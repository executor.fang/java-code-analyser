package lerhyd;

import java.util.Map;

public class Solution<Node,ValueType> {

	private Map<Node,ValueType> inValues;
	private Map<Node,ValueType> outValues;

	public Solution(Map<Node,ValueType> inValues, Map<Node,ValueType> outValues) {
		this.inValues = inValues;
		this.outValues = outValues;
	}

	public ValueType getValueBefore(Node node) {
		return inValues.get(node);
	}

	public ValueType getValueAfter(Node node) {
		return outValues.get(node);
	}
}
